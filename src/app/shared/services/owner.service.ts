import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OwnerService {

  constructor(private http: HttpClient) { }

  getOwnerDetail(body: any): Observable<any> {
    return this.http.get(environment.apiUrl + '/owner/?ownerID=' +body);
  }

  editProfile(ownerId, data): Observable<any> {
    return this.http.put(environment.apiUrl + '/owner/edit-profile?OwnerID=' + ownerId, data);
  }


}
