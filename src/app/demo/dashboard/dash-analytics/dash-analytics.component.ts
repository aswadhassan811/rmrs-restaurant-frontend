import { Component, OnInit, ViewChild } from '@angular/core';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { ChartDB } from '../../../fack-db/chart-data';
import { ChartComponent, ApexAxisChartSeries, ApexChart, ApexXAxis, ApexTitleSubtitle, ApexDataLabels, ApexStroke, ApexTooltip } from "ng-apexcharts";

import { ApexChartService } from '../../../theme/shared/components/chart/apex-chart/apex-chart.service';
export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  title: ApexTitleSubtitle;
  dataLabels: ApexDataLabels;
  stroke: ApexStroke;
  tooltip: ApexTooltip
};
@Component({
  selector: 'app-dash-analytics',
  templateUrl: './dash-analytics.component.html',
  styleUrls: ['./dash-analytics.component.scss']
})
export class DashAnalyticsComponent implements OnInit {
  @ViewChild("chart", { static: false }) chart: ChartComponent;

  public chartDB: Partial<ChartOptions>;

  public lastDate: number;
  public line2CAC: any;
  public data: any;

  public intervalSub: any;
  public intervalMain: any;

  public dailyVisitorStatus: string;
  public dailyVisitorAxis: any;

  public deviceProgressBar: any;
  countofrestaurant: any;
  totalNoOfOders: any;
  OwnerId: any;
  MonthlyAmountData: any;
  RestaurantList: any;
  RestaurantsCount: any;
  orderList: any;
  RestaurantID: any;
  DashboardDataMonthlyAmount: any;
  ordersCount: number = 0;
  DashboardData: any;
  MonthlyOrderData: any;
  CumulativeCurrentYearData: { TotalInvoices: any; TotalRestaurants: any; TotalSalesTax: any; };
  DashboardDataMonthlyOrders: any;
  activeClassL: any;
  activeClassC: string;
  constructor(public apexEvent: ApexChartService, private restaurantService: RestaurantService) {

    this.dailyVisitorStatus = '1y';

    this.chartDB = {
      chart: {
        height: 350,
        type: 'area',
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      tooltip: {
        x: {
          format: 'dd/MM/yy HH:mm'
        },
      }
    };
  }


  ngOnInit() {
    this.countofrestaurant = localStorage.getItem("countOfRestaurant");
    this.totalNoOfOders = localStorage.getItem("TotalOrders");
    this.OwnerId = localStorage.getItem("OwnerID");
    this.RestaurantID = localStorage.getItem("restaurantIdforProfile");
    this.getDasboardData();
  }
  getDasboardData() {
    this.restaurantService.Dashboard(this.OwnerId).subscribe(
      data => {
        this.DashboardData = data['data'];
        console.log("Dashboard Data:", this.DashboardData)
        let x_axis: any = []
        let y1_axis: any = []
        let y2_axis: any = []
        this.DashboardDataMonthlyAmount = data['data']['MonthlyRevenue'];
        console.log("DashboardDataMonthlyAmount", this.DashboardDataMonthlyAmount)
        for (let i = 0; i < this.DashboardDataMonthlyAmount.length; i++) {
          x_axis.push(this.DashboardDataMonthlyAmount[i]._id)
          y1_axis.push(this.DashboardDataMonthlyAmount[i].TotalAmount)
         // y2_axis.push(this.DashboardDataMonthlyAmount[i].TotalAmount)
        }
        this.MonthlyAmountData = [{
          name: 'Sales Amount',
          data: y1_axis
        },
        {
          name: '',
          data: y2_axis
        },
         {
          categories: x_axis,
        }]
        console.log("Dashboard last year Data: ", this.DashboardDataMonthlyAmount);
        this.TotalSales();
        debugger
        let Cx_axis: any = []
        let Cy1_axis: any = []
        let Cy2_axis: any = []
        this.DashboardDataMonthlyOrders = data['data']['MonthlyRevenue'];
        for (let i = 0; i < this.DashboardDataMonthlyOrders.length; i++) {
          Cx_axis.push(this.DashboardDataMonthlyOrders[i]._id)
          Cy1_axis.push(this.DashboardDataMonthlyOrders[i].Count)
         // Cy2_axis.push(this.DashboardDataMonthlyOrders[i].Count)
        }
        // this.CumulativeCurrentYearData = {
        //   TotalInvoices: data['data']['ThisYear']['TotalInvoices'],
        //   TotalRestaurants: data['data']['ThisYear']['TotalRestaurants'],
        //   TotalSalesTax: data['data']['ThisYear']['TotalSalesTax']
        // }

        this.MonthlyOrderData = [{
          name: 'Total orders',
          data: Cy1_axis
        }, {
          name: '',
          data: Cy2_axis
        }, {
          categories: Cx_axis,
        }]
      },
      err => {

      }
    );

  }
  TotalSales() {
    this.chartDB.series = [this.MonthlyAmountData[0], this.MonthlyAmountData[1]]
    this.chartDB.xaxis = this.MonthlyAmountData[2]
    this.activeClassL = "active";
    this.activeClassC = "";
  }
  TotalOrders() {
    this.chartDB.series = [this.MonthlyOrderData[0], this.MonthlyOrderData[1]]
    this.chartDB.xaxis = this.MonthlyOrderData[2]
    this.activeClassL = "";
    this.activeClassC = "active";
  }
  ngOnDestroy() {
    if (this.intervalSub) {
      clearInterval(this.intervalSub);
    }
    if (this.intervalMain) {
      clearInterval(this.intervalMain);
    }
  }
  restaurantOrder() {
    this.restaurantService.RestaurantOrder(this.RestaurantID).subscribe(
      data => {
        this.orderList = data['data'];
        this.ordersCount = (this.ordersCount) + (this.orderList.length);
      },
      err => {
      }
    );
  }
}
