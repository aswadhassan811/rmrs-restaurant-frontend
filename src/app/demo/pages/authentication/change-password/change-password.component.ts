import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ConfirmedValidator } from 'src/app/shared/confirmed.validator';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private router: Router, private authService: AuthService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.changePasswordForm =  this.formBuilder.group({
      CreatePassword: new FormControl('', [
        Validators.required, Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')]),
      ConfirmPassword: new FormControl('', [
        Validators.required,]),
    }, {
      validator: ConfirmedValidator('CreatePassword', 'ConfirmPassword')
    });
  }
  showSuccess() {
    this.toastr.success('password updated succesfully','Success', {
      timeOut: 3000,
      'progressBar': true,
    });
  }
  showError() {
    this.toastr.error('Change Password not successful','Error', {
      timeOut: 3000,
      'progressBar': true,
    });
  }

  changePassword(){
    let changeBody = {
      OwnerID: localStorage.getItem('OwnerID'),
      Password: this.changePasswordForm.value['CreatePassword']
    }
    this.authService.changePassword(changeBody).subscribe(
      data => {
        console.log('changed password', data)
        this.showSuccess();
        this.router.navigateByUrl('/dashboard/analytics')
      },
      err => {
        console.log('changed password', err)
        this.showError();
      }
    )
  }
}
