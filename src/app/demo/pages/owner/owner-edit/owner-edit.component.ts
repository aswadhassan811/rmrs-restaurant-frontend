import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { OwnerService } from 'src/app/shared/services/owner.service';

@Component({
  selector: 'app-owner-edit',
  templateUrl: './owner-edit.component.html',
  styleUrls: ['./owner-edit.component.scss']
})
export class OwnerEditComponent implements OnInit {
  editProfileForm: FormGroup;

  ownerId;

  constructor(
    private fb: FormBuilder,
    private ownerService: OwnerService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.renderForm();

    /// getting logged in user details
    this.ownerId = localStorage.getItem('OwnerID');

    ///getting user details
    this.getProfile();
  }

  renderForm() {
    this.editProfileForm = this.fb.group({
      FullName: ['', [Validators.required]],
      FatherName: ['', [Validators.required]],
      Email: [{value: '', disabled: true}, [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      // CreatePassword: ['1234567', [Validators.required, Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')]],
      // DOB: ['', [Validators.required]],
      Address: ['', [Validators.required]],
      PhoneNo: [''],
      MobileNo: ['', [Validators.required,  Validators.pattern(".{10,10}")]],
      CNICNo: ['', [Validators.required, Validators.pattern("^[0-9]{13}$")]]
    });
  }

  getProfile() {
    const body = this.ownerId;;
    this.ownerService.getOwnerDetail(body).subscribe(
      data => {
        if (data.Message == 'Success') {
          // patch value to form
          this.populateForm(data.data);
        }
        if (data.Message == 'Failure'){
          // this.showErrorForVerifyEmail();
        }
      },
  
      err => {
        console.log(err);
        // this.showError();
        // this.error = err.error.data.description;
      }
    );
  }

  populateForm(data) {
    if (data) {
      // console.log(data);
      this.editProfileForm.controls['FullName'].setValue(data.OwnerName);
      this.editProfileForm.controls['FatherName'].setValue(data.FathersName);
      this.editProfileForm.controls['Email'].setValue(data.Email);
      
      // this.editProfileForm.controls['CreatePassword'].setValue(data.Password);
      // this.editProfileForm.controls['DOB'].setValue(data.DOB);
      this.editProfileForm.controls['Address'].setValue(data.Address);
      this.editProfileForm.controls['PhoneNo'].setValue(data.PhoneNumber);
      this.editProfileForm.controls['MobileNo'].setValue(data.MobileNumber);
      this.editProfileForm.controls['CNICNo'].setValue(data.CNIC);
    }
  }

  updateProfile() {
    /// checking for invalid form
    if (this.editProfileForm.invalid) {
      this.editProfileForm.markAllAsTouched();
      return ;
    }

    const formData = {
      OwnerName: this.editProfileForm.value.FullName,
      FathersName: this.editProfileForm.value.FatherName,
      CNIC: this.editProfileForm.value.CNICNo,
      Address: this.editProfileForm.value.Address,
      PhoneNumber: this.editProfileForm.value.PhoneNo,
      MobileNumber: this.editProfileForm.value.MobileNo
    }

    this.ownerService.editProfile(this.ownerId, formData).subscribe(
      data => {
        // console.log(data);
        if (data.Message == 'Success') {
          this.showSuccess();

          // navigating back
          this.router.navigate(['/owner/owner-profile'])
        }
        if (data.Message == 'Failure'){
          this.showError();
        }
      },
  
      err => {
        console.log(err);
        this.showError();
        // this.showError();
        // this.error = err.error.data.description;
      }
    );
  }

  showSuccess() {
    this.toastr.success('Profile Update Successfully','Success', {
      timeOut: 3000,
      'progressBar': true,
    });
  }

  showError() {
    this.toastr.error('Something went wrong, Please try again','Error', {
      timeOut: 3000,
      'progressBar': true,
    });
  }

}
