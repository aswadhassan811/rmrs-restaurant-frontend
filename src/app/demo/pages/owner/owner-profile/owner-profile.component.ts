import { Component, OnInit } from '@angular/core';
import { OwnerService } from 'src/app/shared/services/owner.service';

@Component({
  selector: 'app-owner-profile',
  templateUrl: './owner-profile.component.html',
  styleUrls: ['./owner-profile.component.scss']
})
export class OwnerProfileComponent implements OnInit {
  ownerData: any;
  constructor(private ownerService: OwnerService) { }

  ngOnInit(): void {
    this.ownerDetails();
  }

  ownerDetails(){
    let ownerId = localStorage.getItem("OwnerID")
    this.ownerService.getOwnerDetail(ownerId).subscribe(
      data => {
        this.ownerData = data.data;
        // console.log('Owner Details:', data)
      },
      err => {
        console.log('Owner Details:', err)
      }
    )
  }

}
