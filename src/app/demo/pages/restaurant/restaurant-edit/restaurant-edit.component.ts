import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ToastrService } from 'ngx-toastr';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { environment } from 'Theme/src/environments/environment';

@Component({
  selector: 'app-restaurant-edit',
  templateUrl: './restaurant-edit.component.html',
  styleUrls: ['./restaurant-edit.component.scss']
})
export class RestaurantEditComponent implements OnInit {
  restaurantForm: FormGroup;
  discountForm: FormGroup;
  deliveryForm: FormGroup;
  timingForm: FormGroup;

  dropdownSettings: IDropdownSettings;

  restaurantDetails: any = {};
  RestaurantID;
  allCategories = [];
  allCities = [];

  fileToUpload: any;
  imageUrl: any;

  OwnerId;

  selectedItem = [];

  deliveryArr: any = {};
  discountsArr = [];
  timingArr = []

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private restaurantService: RestaurantService,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true
    }

    this.getCategories();
    this.getAllCities();
  
    //// getting restaurant id from params
    this.route.params.subscribe(params => {
      if (params.id) {
        ///get restaurant details
        this.RestaurantID = params.id;
        // this.getRestaurantDetails(params['id']);
      }
    })
  }

  ngOnInit(): void {
    /// getting owner id
    this.OwnerId = localStorage.getItem("OwnerID");

    // intializing all forms
    this.renderRestaurantForm();
    this.renderDeliveryForm();
    this.renderDiscountForm();
    this.renderTimingForm();
  }

  renderRestaurantForm() {
    this.restaurantForm = this.formBuilder.group({
      RestaurantName: ['', [Validators.required]],
      Address: ['', [Validators.required,]],
      NTN: ['', [ Validators.required, Validators.pattern(".{9,9}")]],
      ServiceCharges: ['', []],
      CityName: ['', [ Validators.required,]],
      RestaurantCategoryInfo: ['', [ Validators.required,]],
      RestaurantVerified: ['False', [ Validators.required,]],
      AttachImage: ['']
    });
  }

  renderDiscountForm() {
    this.discountForm = this.formBuilder.group({
      DiscountName: ['', [
        Validators.required,
        Validators.pattern("[A-Za-z].{2,}")]],
      DiscountPercentage: ['', [
        Validators.required]],
    });
  }

  renderDeliveryForm() {
    this.deliveryForm = this.formBuilder.group({
      DeliveryTime: ['', [
        Validators.required]],
      DeliveryCharges: ['', [
        Validators.required]],
    });
  }

  renderTimingForm() {
    this.timingForm = this.formBuilder.group({
      MondayStartTime: ['', [
        Validators.required]],
      MondayEndTime: ['', [
        Validators.required]],
      TuesdayStartTime: ['', [
        Validators.required]],
      TuesdayEndTime: ['', [
        Validators.required]],
      WednesdayStartTime: ['', [
        Validators.required]],
      WednesdayEndTime: ['', [
        Validators.required]],
      ThursdayStartTime: ['', [
        Validators.required]],
      ThursdayEndTime: ['', [
        Validators.required]],
      FridayStartTime: ['', [
        Validators.required]],
      FridayEndTime: ['', [
        Validators.required]],
      SaturdayStartTime: ['', [
        Validators.required]],
      SaturdayEndTime: ['', [
        Validators.required]],
      SundayStartTime: ['', [
        Validators.required]],
      SundayEndTime: ['', [
        Validators.required]],
    });
  }

  getCategories() {
    this.restaurantService.getCategoryName()
    .subscribe( (response: any) => {
        if (response.Message === 'Success') {
          this.allCategories = response.data;
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  getAllCities() {
    this.restaurantService.getCity()
    .subscribe( (data: any) => {
      if (data.Message == 'Success') {
        let cities_data = []
        for (let i = 0; i < data['data'].length; i++) {
          // console.log(data['data'])
          cities_data.push({ item_id: data['data'][i].CityID, item_text: data['data'][i].CityName })
        }

        this.allCities = cities_data;
        this.getRestaurantDetails(this.RestaurantID);
        // console.log("Get City List: ", this.allCities);
      }
      },
      err => {
        console.log(err);
      }
    );
  }

  getRestaurantDetails(id) {
    if (id) {
      this.restaurantService.restaurantView(id)
        .subscribe((response: any) => {
          // console.log(response);
          if (response.Message == 'Success') {
            this.restaurantDetails = response.data;
            this.populateRestaurantForm(response.data);
          }
        },
        error => {
          console.log(error);
        })
    }
  }

  populateRestaurantForm(data) {
    console.log(data);
    if (data) {
      this.restaurantForm.controls['RestaurantName'].patchValue(data.RestaurantName);
      this.restaurantForm.controls['Address'].patchValue(data.Address);
      this.restaurantForm.controls['NTN'].patchValue(data.NTNNumber);
      this.restaurantForm.controls['RestaurantCategoryInfo'].patchValue(data.CategoryID);
      this.restaurantForm.controls['RestaurantVerified'].patchValue(data.RestaurantVerified);
      this.restaurantForm.controls['ServiceCharges'].patchValue(data.ServiceCharges);

      /// finding selected city
      const foundCity = this.allCities.filter(x => x.item_text == data.City);
      if (foundCity.length) {
        this.restaurantForm.controls['CityName'].patchValue(foundCity);
      }

      this.imageUrl = data.Image;

      // for delivery
      this.deliveryArr = data.Delivery;
      this.discountsArr = data.Discounts;
      this.timingArr = data.Timings;
    }
  }

  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);

    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  Uploadimage() {
    this.restaurantService.postFile(this.fileToUpload).subscribe(
      data => {
        this.imageUrl = data['Imageurl'];
      }
    );
  }

  updateRestaurant() {
    if (this.restaurantForm.invalid) {
      this.restaurantForm.markAllAsTouched();
      return;
    }
   
    if (this.restaurantForm.controls['ServiceCharges'].value == "") {
      this.restaurantForm.controls['ServiceCharges'].setValue(0);
    }

    let body =
    {
      //RestaurantID: this.RestaurantId,
      RestaurantName: this.restaurantForm.controls['RestaurantName'].value,
      Address: this.restaurantForm.controls['Address'].value,
      OwnerID: this.OwnerId,
      NTNNumber: this.restaurantForm.controls['NTN'].value,
      RestaurantCategory: this.restaurantForm.controls['RestaurantCategoryInfo'].value,
      ServicesCharges: this.restaurantForm.controls['ServiceCharges'].value,
      RestaurantVerified: this.restaurantForm.controls['RestaurantVerified'].value,
      CategoryID: this.restaurantForm.controls['RestaurantCategoryInfo'].value,
      Discounts: this.discountsArr,
      Delivery: this.deliveryArr,
      Timings: this.timingArr,
      Image: this.imageUrl
    }
    this.restaurantService.updateRestaurant(body, this.RestaurantID).subscribe(
      data => {
        if (data['Message'] == "Success") {
          this.showSuccessForUpdate();
        }
        this.restaurantForm.reset();
        this.discountForm.reset();
        this.deliveryForm.reset();
       
        this.router.navigate(['/restaurant/restaurant-list']);
      },
      err => {
        this.showErrorForUpdate();
        // this.error = err.error.data.description;
        // this.loading = false;

      });
  }

  discountFormFunc() {
    //this.replaced = false;
    let body = {
      DiscountName: this.discountForm.controls['DiscountName'].value,
      DiscountPercentage: this.discountForm.controls['DiscountPercentage'].value,
    }
  
    this.discountsArr.push(body);
    this.discountForm.reset();
  }

  addDeliveryFormFunc() {
    let body = {
      DeliveryTime: this.deliveryForm.controls['DeliveryTime'].value,
      DeliveryCharges: this.deliveryForm.controls['DeliveryCharges'].value,
    }

    this.deliveryArr = body;
    this.deliveryForm.reset();
  }

  setTimingsFunc() {

    let body = [
      {
        Day: "Monday",
        StartTime: this.timingForm.controls['MondayStartTime'].value,
        EndTime: this.timingForm.controls['MondayEndTime'].value
      },
      {
        Day: "Tuesday",
        StartTime: this.timingForm.controls['TuesdayStartTime'].value,
        EndTime: this.timingForm.controls['TuesdayEndTime'].value
      },
      {
        Day: "Wednesday",
        StartTime: this.timingForm.controls['WednesdayStartTime'].value,
        EndTime: this.timingForm.controls['WednesdayEndTime'].value
      },
      {
        Day: "Thursday",
        StartTime: this.timingForm.controls['ThursdayStartTime'].value,
        EndTime: this.timingForm.controls['ThursdayEndTime'].value
      },
      {
        Day: "Friday",
        StartTime: this.timingForm.controls['FridayStartTime'].value,
        EndTime: this.timingForm.controls['FridayEndTime'].value
      },
      {
        Day: "Saturday",
        StartTime: this.timingForm.controls['SaturdayStartTime'].value,
        EndTime: this.timingForm.controls['SaturdayEndTime'].value
      },
      {
        Day: "Sunday",
        StartTime: this.timingForm.controls['SundayStartTime'].value,
        EndTime: this.timingForm.controls['SundayEndTime'].value
      }
    ]
    
    this.timingArr = body;
  }

  showSuccessForUpdate() {
    this.toastr.success('', 'Restaurant Updated Successfully', {
      timeOut: 3000,
      'progressBar': true,
    });
  }

  showErrorForUpdate() {
    this.toastr.error('', 'Restaurant Not Updated!', {
      timeOut: 3000,
      'progressBar': true,
    });
  }
}
