import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/shared/services/menu.service';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.scss']
})
export class ReservationListComponent implements OnInit {
  Reservation: any;
  cancel: boolean;
  abc: boolean = false;
  RestaurantID: any;
  reservationList: any;
  reservationStatus: any;
  awaitingStatus : string = "AWAITING_ACCEPTANCE";
  reservedStatus : string = "RESERVED";
  confirmedStatus : string = "CONFIRMED";
  rejectedStatus : string = "REJECTED";
  cancelledStatus : string = "CANCELLED";
  Descrpition: any;
  ReservationID: any;
  constructor(private menuService: MenuService, private router: Router, private restaurantService: RestaurantService) { }

  ngOnInit(): void {
    this.Reservation = this.menuService.reservationFields;
    this.cancel = this.menuService.cancelD;
    this.RestaurantID = localStorage.getItem("restaurantIdforProfile");
    this.viewReservation();
  }
  addReservation() {
    this.menuService.decision(this.abc);
    this.router.navigate(['/reservation/reservation-add']);
  }
  viewReservation() {
    this.restaurantService.ReservationList(this.RestaurantID).subscribe(
      data => {
        this.reservationList = data['data'].result;
        console.log('Reservation list', this.reservationList)
        this.reservationList.reverse();
      },
      err => {
      }
    );
  }
  rejectReservation(Reservationid) {
    this.ReservationID = Reservationid;
    let body = {
      ReservationID : Reservationid,
      CurrentStatus : this.awaitingStatus,
      UpdatedStatus:  this.rejectedStatus
    }
     this.restaurantService.rejectReservation(body).subscribe(
       data => {
        this.Descrpition = data['data'].description;
        this.ngOnInit();
       },
       err => {

       }
     );
  }
  cancelReservation(Reservationid) {
    this.ReservationID = Reservationid;
    let body = {
      ReservationID : Reservationid,
      CurrentStatus : this.reservedStatus,
      UpdatedStatus:  this.cancelledStatus
    }
    console.log('cancelReservation', body)
    debugger;
    console.log('cancelReservation', body)
     this.restaurantService.rejectReservation(body).subscribe(
       data => {
        this.Descrpition = data['data'].description;
        this.ngOnInit();
       },
       err => {

       }
     );
  }
  acceptReservation(Reservationid) {
    let body = {
      ReservationID : Reservationid,
      CurrentStatus : this.awaitingStatus,
      UpdatedStatus:  this.reservedStatus
    }
     this.restaurantService.acceptReservationForCustomer(body).subscribe(
       data => {
        this.Descrpition = data['data'].description;
        this.ngOnInit();
       },
       err => {

       }
     );
  }
  arrivedtReservation(Reservationid) {
    let body = {
      ReservationID : Reservationid,
      CurrentStatus : this.reservedStatus,
      UpdatedStatus:  this.confirmedStatus
    }
     this.restaurantService.acceptReservationForCustomer(body).subscribe(
       data => {
        this.Descrpition = data['data'].description;
        this.ngOnInit();
       },
       err => {

       }
     );
  }
}
