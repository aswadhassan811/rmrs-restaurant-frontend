Prerequisite:
1) Download and Install Node js latest stable version.

2) Confirm node and npm installation by writing this commands in terminal
 ------->>>>  node -v & npm -v

3) To install Angular globally Open Terminal and write this command
   ------>>>>>> npm install -g @angular/cli

4) Download and install your favourite IDE (e.g Visual Studio Code Editor).

5) Download and install git.

4) Done.


Installation guide for Angular

Step 1: Install Node.js link: https://nodejs.org/en/download/

Step 2: Install TypeScript (Optional)

Step 3: Install Angular CLI
2. command npm install -g @angular/cli

Reference link: https://ccbill.com/kb/install-angular-on-windows
